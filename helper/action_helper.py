from typing import List

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QWidget
from qgis.gui import QgisInterface


class ActionHelper:
    def __init__(self, iface: QgisInterface):
        self.iface = iface

    def add_action(
            self,
            icon_path: str,
            text: str,
            callback,
            enabled_flag: bool = True,
            add_to_menu: bool = True,
            add_to_toolbar: bool = True,
            status_tip: str = None,
            whats_this: str = None,
            menu: str = 'Blank Action',
            parent: QWidget = None) -> QAction:

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(menu, action)

        return action

    def remove_action(self, actions: List[QAction], menu: str = 'Blank Action'):
        for action in actions:
            self.iface.removePluginVectorMenu(menu, action)
            self.iface.removeToolBarIcon(action)
