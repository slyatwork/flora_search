from typing import List

import requests
from PyQt5.QtCore import QSettings
from qgis.core import QgsNetworkAccessManager, QgsApplication, QgsAuthMethodConfig, QgsAuthManager
from qgis.gui import QgisInterface

from ..constants.connection_constants import TEST_URL, AUTH_CONFIG
from ..i18n.translator import Translator


class ConnectionHelper:
    def __init__(self, iface: QgisInterface, translator: Translator):
        self.translator = translator
        self.iface = iface

        network_manager = QgsNetworkAccessManager()
        network_manager.setupDefaultProxyAndCache()
        if QSettings().value(u'proxy/proxyEnabled') == 'true':
            self.proxies = {'http': QSettings().value(u'proxy/proxyHost') + ':' + QSettings().value(u'proxy/proxyPort')}
        else:
            self.proxies = ''

    def test_geoserver_connection(self) -> bool:
        cbnbp_data_translation = self.translator.translate('CBNBP Data')
        geoserver_not_reachable_translation = self.translator.translate('CBNBP geoserver is not reachable')
        try:
            r = requests.get(TEST_URL, timeout=10, proxies=self.proxies)
        except ConnectionRefusedError:
            self.iface.messageBar().pushWarning(cbnbp_data_translation, geoserver_not_reachable_translation)
            return False
        if r.status_code != 200:
            self.iface.messageBar().pushWarning(cbnbp_data_translation, geoserver_not_reachable_translation)
            return False
        else:
            self.iface.messageBar().pushInfo(cbnbp_data_translation,
                                             self.translator.translate('you are well connected to the CBNBP geoserver'))
            return True

    def test_authentication_config(self) -> bool:
        auth_manager: QgsAuthManager = QgsApplication.authManager()
        config_ids: List[str] = auth_manager.configIds()
        try:
            config_ids.index(AUTH_CONFIG)
        except ValueError:
            self.iface.messageBar().pushWarning(self.translator.translate('Authentication'),
                                                self.translator.translate('consultation profile does not exist'))
            return False
        self.iface.messageBar().pushInfo(self.translator.translate('Authentication'),
                                         self.translator.translate('you are well authenticated'))
        return True

    def create_authentication_profile(self):
        auth_manager = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setMethod('Basic')
        config.setName('flora_search')
        config.setId(AUTH_CONFIG)
        config.setConfig('username', 'edition')
        config.setConfig('password', 'cbnbp1418')
        auth_manager.storeAuthenticationConfig(config)
        self.iface.messageBar().pushInfo(self.translator.translate('Authentication'),
                                         self.translator.translate('consultation profile has been created'))
