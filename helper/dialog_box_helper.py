class DialogBoxHelper:
    def __init__(self, dialog_box):
        #: dialog_box is a FloraSearchDialog
        self.dialog_box = dialog_box

    def clean_dialog_box(self, choice: str):
        if choice == 'lb_nom':
            self.dialog_box.extent_box.setEnabled(False)
            self.dialog_box.label_taxon_text.setEnabled(True)
            self.dialog_box.taxon.setEnabled(True)
            self.dialog_box.label_departments.setEnabled(True)
            self.dialog_box.departments.setEnabled(True)
            self.dialog_box.label_taxon_code.setEnabled(False)
            self.dialog_box.label_taxon_code_precision.setEnabled(False)
            self.dialog_box.cd_nom.setEnabled(False)

        if choice == 'cd_nom':
            self.dialog_box.extent_box.setEnabled(False)
            self.dialog_box.label_taxon_text.setEnabled(False)
            self.dialog_box.taxon.setEnabled(False)
            self.dialog_box.label_departments.setEnabled(True)
            self.dialog_box.departments.setEnabled(True)
            self.dialog_box.label_taxon_code.setEnabled(True)
            self.dialog_box.label_taxon_code_precision.setEnabled(True)
            self.dialog_box.cd_nom.setEnabled(True)

        if choice == 'polygon':
            self.dialog_box.extent_box.setEnabled(False)
            self.dialog_box.label_taxon_text.setEnabled(False)
            self.dialog_box.taxon.setEnabled(False)
            self.dialog_box.label_departments.setEnabled(True)
            self.dialog_box.departments.setEnabled(True)
            self.dialog_box.label_taxon_code.setEnabled(True)
            self.dialog_box.label_taxon_code_precision.setEnabled(True)
            self.dialog_box.cd_nom.setEnabled(True)

        if choice == 'extent':
            self.dialog_box.extent_box.setEnabled(True)
            self.dialog_box.label_taxon_text.setEnabled(False)
            self.dialog_box.taxon.setEnabled(False)
            self.dialog_box.label_departments.setEnabled(False)
            self.dialog_box.departments.setEnabled(False)
            self.dialog_box.label_taxon_code.setEnabled(False)
            self.dialog_box.label_taxon_code_precision.setEnabled(False)
            self.dialog_box.cd_nom.setEnabled(False)
