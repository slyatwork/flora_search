from typing import List, Optional
from urllib import parse

from qgis.core import QgsVectorLayer
from qgis.gui import QgisInterface

from ..constants.connection_constants import URL, PARAMS
from ..constants.layer_constants import LIST_DEPARTMENT, QUERY_FLORA_LB_NOM_COMMUNE, \
    QUERY_FLORA_CD_NOM_COMMUNE, QUERY_FLORA_CD_NOM_POLYGON, QUERY_FLORA_EXTENT_LAYER, QUERY_FLORA_EXTENT_DATA
from ..i18n.translator import Translator


class LayerBuilder:
    def __init__(self, iface: QgisInterface, translator: Translator):
        self.iface = iface
        self.translator = translator

    def build_vector_layer(self, uri: str, name: str = 'Flora', provider: str = 'WFS') -> QgsVectorLayer:
        layer = QgsVectorLayer(uri, name, provider)
        self.iface.messageBar().pushInfo(self.translator.translate('Called layer uri'), uri)
        if not layer.isValid():
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('null layer : ') + name)
        return layer

    def build_department_layer(self) -> QgsVectorLayer:
        params = PARAMS.copy()
        params.update(typename=LIST_DEPARTMENT)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, 'DEPARTMENT')

    def build_flora_by_lb_nom_layer(self, taxon: str, departments: List[str], name: str) -> Optional[QgsVectorLayer]:
        if not departments or not taxon:
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('empty values !'))
            return None
        params = PARAMS.copy()
        view_params = 'LB_NOM:\'%' + taxon + '%\';CD_DEPT:\'' + '_'.join(departments) + '\''
        params.update(typename=QUERY_FLORA_LB_NOM_COMMUNE, viewparams=view_params)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, name)

    def build_flora_by_cd_nom_layer(self, taxons: List[str], departments: List[str], name: str) \
            -> Optional[QgsVectorLayer]:
        if not departments or not taxons:
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('empty values !'))
            return None
        params = PARAMS.copy()
        view_params = 'CD_NOM:\'' + '_'.join(taxons) + '\';CD_DEPT:\'' + '_'.join(departments) + '\''
        params.update(typename=QUERY_FLORA_CD_NOM_COMMUNE, viewparams=view_params)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, name)

    def build_flora_by_cd_nom_polygon_layer(self, taxons: List[str], departments: List[str], name: str) \
            -> Optional[QgsVectorLayer]:
        if not departments or not taxons:
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('empty values !'))
            return None
        params = PARAMS.copy()
        view_params = 'CD_NOM:\'' + '_'.join(taxons) + '\';CD_DEPT:\'' + '_'.join(departments) + '\''
        params.update(typename=QUERY_FLORA_CD_NOM_POLYGON, viewparams=view_params)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, name)

    def build_flora_by_extent_layer(self, xbg: int, ybg: int, xhd: int, yhd: int, name: str) \
            -> Optional[QgsVectorLayer]:
        if not xbg or not ybg or not xhd or not yhd:
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('empty values !'))
            return None
        params = PARAMS.copy()
        view_params = 'XBG:' + str(xbg) + ';YBG:' + str(ybg) + ';XHD:' + str(xhd) + ';YHD:' + str(yhd)
        params.update(typename=QUERY_FLORA_EXTENT_LAYER, viewparams=view_params)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, name)

    def build_flora_by_extent_data(self, xbg: int, ybg: int, xhd: int, yhd: int, name: str) \
            -> Optional[QgsVectorLayer]:
        if not xbg or not ybg or not xhd or not yhd:
            self.iface.messageBar().pushWarning(self.translator.translate('CBNBP Data'),
                                                self.translator.translate('empty values !'))
            return None
        params = PARAMS.copy()
        view_params = 'XBG:' + str(xbg) + ';YBG:' + str(ybg) + ';XHD:' + str(xhd) + ';YHD:' + str(yhd)
        params.update(typename=QUERY_FLORA_EXTENT_DATA, viewparams=view_params)
        uri = URL + parse.unquote(parse.urlencode(params))
        return self.build_vector_layer(uri, name + '_DATA')
