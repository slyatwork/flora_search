from typing import List

from qgis.core import QgsVectorLayer
from qgis.gui import QgisInterface

from ..builder.layer_builder import LayerBuilder
from ..i18n.translator import Translator
from ..interface.flora_search_dialog import FloraSearchDialog


class LayerFactory:
    def __init__(self, iface: QgisInterface, translator: Translator):
        self.iface = iface
        self.translator = translator
        self.layer_builder = LayerBuilder(self.iface, self.translator)

    def build_layers(self, dialog_box: FloraSearchDialog) -> List[QgsVectorLayer]:
        if dialog_box.choice == 'lb_nom':
            departments = [department.text()[-3:][:2] for department in dialog_box.departments.selectedItems()]
            layer_name = dialog_box.layer_name.text() if dialog_box.layer_name.text() else 'Flora'
            return [self.layer_builder.build_flora_by_lb_nom_layer(dialog_box.taxon.text(), departments, layer_name)]

        if dialog_box.choice == 'cd_nom':
            taxons = [taxon.rstrip('\n').lstrip('\n').rstrip(' ').lstrip(' ')
                      for taxon in dialog_box.cd_nom.toPlainText().split(',')]
            departments = [department.text()[-3:][:2] for department in dialog_box.departments.selectedItems()]
            layer_name = dialog_box.layer_name.text() if dialog_box.layer_name.text() else 'Flora'
            return [self.layer_builder.build_flora_by_cd_nom_layer(taxons, departments, layer_name)]

        if dialog_box.choice == 'polygon':
            taxons = [taxon.rstrip('\n').lstrip('\n').rstrip(' ').lstrip(' ')
                      for taxon in dialog_box.cd_nom.toPlainText().split(',')]
            departments = [department.text()[-3:][:2] for department in dialog_box.departments.selectedItems()]
            layer_name = dialog_box.layer_name.text() if dialog_box.layer_name.text() else 'Flora'
            return [self.layer_builder.build_flora_by_cd_nom_polygon_layer(taxons, departments, layer_name)]

        if dialog_box.choice == 'extent':
            extent = dialog_box.extent_box.outputExtent()
            layer_name = dialog_box.layer_name.text() if dialog_box.layer_name.text() else 'Flora'
            xbg, ybg, xhd, yhd = int(extent.xMinimum()), int(extent.yMinimum()), \
                                 int(extent.xMaximum()), int(extent.yMaximum())
            extent_layer = self.layer_builder.build_flora_by_extent_layer(xbg, ybg, xhd, yhd, layer_name)
            data_layer = self.layer_builder.build_flora_by_extent_data(xbg, ybg, xhd, yhd, layer_name)
            return [extent_layer, data_layer]
