<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
    <context>
        <name>FloraSearch</name>
        <message>
            <source>Good morning</source>
            <translation>Bonjour</translation>
        </message>
        <message>
            <source>Search in Flora DB</source>
            <translation>Chercher dans la base Flora</translation>
        </message>
        <message>
            <source>Flora Search</source>
            <translation>Recherche Flora</translation>
        </message>
        <message>
            <source>CBNBP Data</source>
            <translation>Données CBNBP</translation>
        </message>
        <message>
            <source>CBNBP geoserver is not reachable</source>
            <translation>Le geoserver du CBNBP n'est pas joignable</translation>
        </message>
        <message>
            <source>you are well connected to the CBNBP geoserver</source>
            <translation>vous êtes bien connecté au geoserver du CBNBP</translation>
        </message>
        <message>
            <source>Authentication</source>
            <translation>Authentification</translation>
        </message>
        <message>
            <source>consultation profile does not exist</source>
            <translation>le profil de consultation n'existe pas dans la base QGis</translation>
        </message>
        <message>
            <source>you are well authenticated</source>
            <translation>vous êtes bien authentifié</translation>
        </message>
        <message>
            <source>consultation profile has been created</source>
            <translation>le profil de consultation a bien été créé</translation>
        </message>
        <message>
            <source>Called layer uri</source>
            <translation>URI de la couche appelée</translation>
        </message>
        <message>
            <source>null layer :</source>
            <translation>couche non valide :</translation>
        </message>
        <message>
            <source>layer(s) added :</source>
            <translation>couche(s) ajoutée(s) :</translation>
        </message>
        <message>
            <source>empty values !</source>
            <translation>certaines valeurs attendues sont vides !</translation>
        </message>
    </context>
    <context>
        <name>FloraSearchDialogBase</name>
        <message>
            <source>Flora Search</source>
            <translation>Recherche Flora</translation>
        </message>
        <message>
            <source>Search in Flora DB</source>
            <translation>Chercher dans la base Flora</translation>
        </message>
        <message>
            <source>Layer name :</source>
            <translation>Nom de la couche :</translation>
        </message>
        <message>
            <source>Request type :</source>
            <translation>Type de requête :</translation>
        </message>
        <message>
            <source>Taxon name contains :</source>
            <translation>Taxon contient :</translation>
        </message>
        <message>
            <source>Department(s) :</source>
            <translation>Département(s) :</translation>
        </message>
        <message>
            <source>Taxon(s) by CD_NOM :</source>
            <translation>Taxon(s) par CD_NOM :</translation>
        </message>
        <message>
            <source>Taxon(s) existence by locality - filtered by LB_NOM and department</source>
            <translation>Répartition de taxon(s) par commune - filtre sur LB_NOM et département(s)</translation>
        </message>
        <message>
            <source>Taxon(s) existence by locality - filtered by CD_NOM and department</source>
            <translation>Répartition de taxon(s) par commune - filtre sur CD_NOM et département(s)</translation>
        </message>
        <message>
            <source>Presence area of taxon(s) - filtered by CD_NOM and department</source>
            <translation>Aire de présence de taxon(s) - flitre sur CD_NOM et département(s)</translation>
        </message>
        <message>
            <source>Known taxons data - filtered by extent</source>
            <translation>Données taxonomiques connues - filtre sur emprise géographique</translation>
        </message>
        <message>
            <source>Please list CD_NOM separated with a comma</source>
            <translation>Lister les CD_NOM en les séparant par une virgule</translation>
        </message>
    </context>
</TS>
