TEST_URL = 'http://cbnbp.mnhn.fr/data/PUBLIC/wms?request=GetCapabilities'

URL = 'http://cbnbp.mnhn.fr/data/SI/wms?'

AUTH_CONFIG = 'flora20'

PARAMS = {
    'authcfg': AUTH_CONFIG,
    'version': '1.0.0',
    'srsname': 'EPSG:2154',
    'format': 'image/png'
}
