#Flora Search - a QGis Plugin
provided by the CBNBP (Conservatoire Botanique National du Bassin Parisien)  

**Requisites**  
- QGis 3+, 3.10 preferred
- Python 3+, 3.7 preferred 

**Recommendations**  
We strongly recommend to use the LTR version of QGis and OSGeo4W64 as installer.

## Install
1. Copy the code in the right place

Consider using OSGeo4W64 as QGis installer, simply deploy this repo in the Python plugins repository.  
For example, if you use the LTR version of QGis :

    git clone <this repo>
    rsync -av <path to cloned repo without slash at the end> <path to OSGeo4W64>/apps/qgis-ltr/python/plugins/ --exclude .git --exclude __pycache__ --delete [--exclude .idea]

2. Declare the plugin in QGis

- Open QGis, in the Plugins menu, click Manage and Install
- Select Flora Search in the Installed Tab
- Close the box

And that's it, Flora Search is fully usable !

## How to use it ?
Flora Search allows to query the CBNBP's Flora DB in 4 possible ways :
- Taxons existence by communes, searched by LB_NOM : get all the communes where an observation at least exists for all taxons corresponding to the text given. Communes polygon are represented.
- Taxons existence by communes, searched by CD_NOM : get all the communes where an observation at least exists for all taxons corresponding to the CD_NOMs given. Communes centroid point are represented.
- Taxons observed by polygon : get all the polygons where an observation at least exists for all taxons corresponding to the CD_NOMs given. 
- Taxons observed in an extent : get all the existing geometries with an observation for an area given. An additional layer composed with data is attached too. 

## Unit test
To launch unit test :
    
    python3 -m unittest discover -t ..

## Notes
- Flora DB is a botanic project lead by the CBNBP to collect, aggregate, store and spread botanic knowledge about flora, fungi and habitats.
- This plugin structure has been generated through Plugin Builder 3 for QGis.

## Licence
This plugin is open source and could be used through CeCILL v2.1 licence.
Feel free to clone, fork and pull requests.  
Please, just mention the CBNBP as authority and Silvère Camponovo as author.  
